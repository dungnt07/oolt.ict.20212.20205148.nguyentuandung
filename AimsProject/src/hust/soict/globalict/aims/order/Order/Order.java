package hust.soict.globalict.aims.order.Order;
import java.util.ArrayList;
import hust.soict.globalict.aims.media.DigitalVideoDisc;
import hust.soict.globalict.aims.media.Media;
import hust.soict.globalict.aims.utils.MyDate.MyDate;

public class Order {
	public static final int MAX_NUMBERS_ORDERED = 10;

	public static final int MAX_LIMITED_ORDER = 5;

	public static int nbOrders = 0;
	
	private ArrayList<Media> itemsOrdered = new ArrayList<Media>();
	
	private MyDate dateOrdered;

	public Order() {
		if (nbOrders == MAX_LIMITED_ORDER) {
			System.out.println("ERROR: Can not make new order. Max number of orders reached!");
			return;
		}
		this.dateOrdered = new MyDate();
		nbOrders++;
	}
	
	public void addMedia(Media m) {
		if (itemsOrdered.size() >= 10) {
			System.out.println("ERROR: Unable to add the item, the order is full.");
			return;
		}
		itemsOrdered.add(m);
		System.out.println("INFO: The item has been added.");
		
		if (itemsOrdered.size() == MAX_NUMBERS_ORDERED)
			System.out.println("WARNING: The order is almost full.");
	}
	
	public void addMedia(ArrayList<Media> mediaList) {
		int i = 0;
		while (itemsOrdered.size() != MAX_NUMBERS_ORDERED && i != mediaList.size()) {
			itemsOrdered.add(mediaList.get(i));
			System.out.println("INFO: The item has been added.");
			i++;
		}
		if (i != mediaList.size()) {
			System.out.println("ERROR: The order is full! The following item(s) can not be added: ");
			for (; i < mediaList.size(); i++) {
				System.out.println(mediaList.get(i).getTitle());
			}
		}
	}
	
	public void removeMedia(Media m) {
		if (!itemsOrdered.contains(m)) {
			System.out.println("ERROR: Unable to remove. Item not found!");
			return;
		}
		itemsOrdered.remove(m);
		System.out.println("INFO: The item has been removed.");
	}
	
	public void removeMedia(int itemID) {
		boolean removeResult = itemsOrdered.removeIf(m -> m.getID() == itemID);
		if (!removeResult)
			System.out.println("Invalid Item ID");
	}
	
	public void printOrder() {
		System.out.println("Current items in the order: ");
		for (Media m : itemsOrdered) {
			System.out.println(String.format("ID: %d %s", m.getID(), m.toString()));
		}
	}

	public void printFinalOrder(){
		System.out.println("***********************Order***********************");
		System.out.println("Date: " + this.dateOrdered.toString());
		this.dateOrdered.print();
		System.out.println("Ordered Items:");
		for (int i = 0; i < itemsOrdered.size() && itemsOrdered.get(i) != null; i++){
			Media item = itemsOrdered.get(i);
			if (item instanceof DigitalVideoDisc) {
				System.out.println(String.format("%d. DVD - %s - %s - %s - %d: %.2f$",
											item.getID(), item.getTitle(), item.getCategory(),
											((DigitalVideoDisc) item).getDirector(), ((DigitalVideoDisc) item).getLength(), 
											item.getCost()));
			} else {
				System.out.print(String.format("%d. Book - %s - %s - %.2f - ", 
											item.getID(), item.getTitle(), item.getCategory(), item.getCost()));
			}
		}
		System.out.println("Total cost: " + this.totalCost());
		System.out.println("***************************************************");
	}


	public float totalCost() {
		float overallCost = 0;
		for (int i = 0; i < itemsOrdered.size(); i++) {
			overallCost += itemsOrdered.get(i).getCost();
		}
		return overallCost - getALuckyItem().getCost();
	}
	
	//TODO: Test the method 
	public Media getALuckyItem() {
		int randPosition = (int) Math.round(Math.random()*(itemsOrdered.size() - 1));
		Media luckyItem = itemsOrdered.get(randPosition);
		System.out.println("Lucky item: " + luckyItem.getTitle());
		return luckyItem;
	}
}
