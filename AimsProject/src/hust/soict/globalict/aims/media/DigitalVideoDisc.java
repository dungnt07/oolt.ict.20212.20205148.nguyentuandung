package hust.soict.globalict.aims.media;
import java.util.Arrays;
import java.util.List;

public class DigitalVideoDisc extends Media {
	private String director;
	private int length;
	
	public String getDirector() {
		return director;
	}
	public void setDirector(String director) {
		this.director = director;
	}
	public int getLength() {
		return length;
	}
	public void setLength(int length) {
		this.length = length;
	}
	public DigitalVideoDisc(String title) {
		super(title);
	}
	
	public DigitalVideoDisc(String title, String category) {
		super(title,category);
	}

	public DigitalVideoDisc(String title, String category, String director) {
		super(title, category);
		this.director = director;
	}
	
	public DigitalVideoDisc(String title, String category, String director, int length, float cost) {
		super(title, category);
		this.director = director;
		this.length = length;
		this.cost = cost;
	}
	
	//TODO: Test the method
	public boolean search(String title) {
		List<String> originTitleArray = Arrays.asList(this.getTitle().split(" "));
		List<String> searchTitleArray  = Arrays.asList(title.split(" "));
		for (String s : searchTitleArray) {
			if (!originTitleArray.contains(s)) {
				return false;
			}
		}
		return true;
	}
	
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(String.format("[Disc] Title: %s, Category: %s, Director: %s, Length: %ds\n", this.getTitle(), this.getCategory(),
									this.getDirector(), this.getLength()));
		return sb.toString();
	}
	
}
