package hust.soict.globalict.aims.media;

public class Media {
	protected static int count = 0;
	protected int ID;
	protected String title;
	protected String category;
	protected float cost;
	
	public int getID() {
		return ID;
	}
	
	public void setID(int ID) {
		this.ID = ID;
	}

	public String getTitle() {
		return title;
	}
	
	public void setTitle(String title) {
		this.title = title;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public float getCost() {
		return cost;
	}

	public void setCost(float cost) {
		this.cost = cost;
	}

	public Media(String title) {
		this.title = title;
		setID(++count);
	}
	
	public Media(String title, String category) {
		this(title);
		this.category = category;
	}

}
