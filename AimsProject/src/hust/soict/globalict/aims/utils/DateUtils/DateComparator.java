package hust.soict.globalict.aims.utils.DateUtils;
import java.time.LocalDate;
import java.util.Comparator;

import hust.soict.globalict.aims.utils.MyDate.MyDate;

public class DateComparator implements Comparator<MyDate>{
	public int compare(MyDate date1, MyDate date2) {
		LocalDate d1 = LocalDate.of(date1.getYear(), date1.getMonth(), date1.getDay());
		LocalDate d2 = LocalDate.of(date2.getYear(), date2.getMonth(), date2.getDay());
		return d1.compareTo(d2);
	}
}
