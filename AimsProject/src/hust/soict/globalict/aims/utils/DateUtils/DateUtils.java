package hust.soict.globalict.aims.utils.DateUtils;
import java.util.Arrays;

import hust.soict.globalict.aims.utils.MyDate.MyDate;

public class DateUtils {
	private static DateComparator comparator = new DateComparator();
	
	public static void compareDates(MyDate date1, MyDate date2) {
		int comp = comparator.compare(date1, date2);
		if (comp == 0)
			System.out.println("Two dates are the same.");
		else if (comp < 0)
			System.out.println(String.format("%s is before %s.",date1.toString(), date2.toString()));
		else
			System.out.println(String.format("%s is after %s.",date1.toString(), date2.toString()));
	}
	
	public static MyDate[] sortDates(MyDate[] dates) {
		MyDate[] datesToSort = Arrays.copyOf(dates, dates.length);
		Arrays.sort(datesToSort, comparator);
		return datesToSort;
	}
	
}
