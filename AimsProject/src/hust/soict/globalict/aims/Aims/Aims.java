package hust.soict.globalict.aims.Aims;

import java.util.ArrayList;
import java.util.Scanner;

import hust.soict.globalict.aims.media.Book;
import hust.soict.globalict.aims.media.DigitalVideoDisc;
import hust.soict.globalict.aims.media.Media;
import hust.soict.globalict.aims.order.Order.Order;

public class Aims {
	@SuppressWarnings({ "unused", "null" })
	public static void main(String[] args) {
		Book b1 = new Book("A tale of Two Cities", "Historical Fiction");
		b1.addAuthor("Charles Dickens");
		Book b2 = new Book("The Hobbit, or There and Back again", "Fantasy Fiction");
		b2.addAuthor("J.R.R. Tolkien");
		Book b3 = new Book("To Kill a Mocking Bird", "Thriller");
		b3.addAuthor("Harper Lee");
		DigitalVideoDisc vid1 = new DigitalVideoDisc("Star Wars", "Science-fiction", "George Lucas", 120, 18.99f);
		DigitalVideoDisc vid2 = new DigitalVideoDisc("The Lion King", "Animation", "Roger Allers", 87, 19.5f);
		DigitalVideoDisc vid3 = new DigitalVideoDisc("Aladdin", "Animation", "John Musker", 90, 18.5f);

		ArrayList<Media> itemList = new ArrayList<Media>();
		itemList.add(b1);
		itemList.add(b2);
		itemList.add(b3);
		itemList.add(vid1);
		itemList.add(vid2);
		itemList.add(vid3);

		Scanner sc = new Scanner(System.in);
		Order order = null;
		boolean orderCreated = false;

		while (true) {
			showMenu();
			int choice;
			choice = sc.nextInt();
			switch (choice) {
			case 0:
				sc.close();
				System.exit(0);
			case 1:
				order = new Order();
				orderCreated = true;
				break;
			case 2:
				if (!orderCreated) {
					System.out.println("Please create a new order first!");
					break;
				}
				int itemChoice;
				listItem(itemList);
				System.out.println("Choose item number: ");
				itemChoice = sc.nextInt();
				if (itemChoice <= 0 || itemChoice > itemList.size()) {
					System.out.println("Invalid index!");
					break;
				}
				order.addMedia(itemList.get(itemChoice - 1));
				break;
			case 3:
				if (!orderCreated) {
					System.out.println("Please create a new order first!");
					break;
				}
				int itemID;
				System.out.println("Enter item ID: ");
				itemID = sc.nextInt();
				order.removeMedia(itemID);
				break;
			case 4:
				if (order == null) {
					System.out.println("Please create a new order first!");
					break;
				}
				order.printOrder();
				break;
			default:
				sc.close();
				System.exit(1);
			}
			
		}
	}
	
	public static void showMenu() {
		System.out.println("Order Management Application: ");
		System.out.println("--------------------------------");
		System.out.println("1. Create new order");
		System.out.println("2. Add item to the order");
		System.out.println("3. Delete item by id");
		System.out.println("4. Display the items list of order");
		System.out.println("0. Exit");
		System.out.println("--------------------------------");
		System.out.println("Please choose a number: 0-1-2-3-4");
	}
	
	public static void listItem(ArrayList<Media> itemList) {
		for (int i = 0; i < itemList.size(); i++) {
			System.out.println(String.format("ID: %d %s", itemList.get(i).getID(), itemList.get(i)));
		}
	}
	
}
