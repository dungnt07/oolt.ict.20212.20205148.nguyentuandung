package hust.soict.globalict.test.utils.DateTest;
import hust.soict.globalict.aims.utils.DateUtils.DateUtils;
import hust.soict.globalict.aims.utils.MyDate.MyDate;

public class DateTest {

	public static void main(String[] args) {
		MyDate date1 = new MyDate();
		System.out.println(date1.toString());
		
		MyDate date2 = new MyDate(4,7,2016);
		date2.printCustomFormat();
		
		MyDate date3 = new MyDate(6,5,2013);
		//date3.accept();
		//System.out.println(date3.toString());
		
		DateUtils.compareDates(date3, date2);
		
		MyDate[] dates = new MyDate[]{date2, date1, date3};
		dates = DateUtils.sortDates(dates);
		for (MyDate date : dates) {
			System.out.println(date);
		}
	}

}
