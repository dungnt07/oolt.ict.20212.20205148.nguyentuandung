package hust.soict.globalict.lab02;
import java.util.Scanner;

public class DaysOfMonth {
	static String[] monthStr = new String[] {"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"};
	static String[] monthAbb = new String[] {"Jan.", "Feb.", "Mar.", "Apr.", "May.", "Jun.", "Jul.", "Aug.", "Sep.", "Oct.", "Nov.", "Dec."};
	static String[] month3char = new String[] {"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};
	
	public static int calculateDaysOfMonth(String month, int year) {
		switch (month) {
		case "January":
		case "March":
		case "May":
		case "July":
		case "August":
		case "October":
		case "December":
			return 31;
		case "February":
			return calculateFebDays(year);
		default:
			return 30;
		}
	}
	
	public static int calculateFebDays(int year) {
		if (year % 400 == 0)
			return 29;
		if (year % 100 == 0)
			return 28;
		if (year % 4 == 0)
			return 29;
		return 28;
	}
	
	public static boolean isInteger(String input) {
		try {
			Integer.parseInt(input);
			return true;
		} 
		catch (NumberFormatException e) {
			return false;
		}
	}
	
	public static String getFullMonth(String monthInput) {
		if (isInteger(monthInput)) {
			return monthStr[Integer.parseInt(monthInput)-1];
		}
		else if (monthInput.length() > 3) {
			return monthInput;
		}
		else {
			for (int i = 0; i < 12; i++) {
				if (monthInput.equals(monthAbb[i]) || monthInput.equals(month3char[i])) 
					return monthStr[i];
			}
		}
		return null;
	}
	
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		System.out.println("Enter year: ");
		int year = scanner.nextInt();
		System.out.println("Enter month: ");
		String monthInput = scanner.next();
		String fullMonthName = getFullMonth(monthInput);
		int numberOfDays = calculateDaysOfMonth(fullMonthName, year);
		System.out.println("Number of days: " + numberOfDays);
		scanner.close();
	}
}
