package hust.soict.globalict.lab02;
import java.util.Scanner;

public class DrawingTriangle {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		System.out.println("Enter the triangle's height: ");
		int h = scanner.nextInt();
		int padWidth = h - 1;
		for (int i = 1; i <= h; i++) {
			for (int k = 0; k < padWidth; k++) {
				System.out.print(' ');
			}
			for (int j = 0; j < 2*i - 1; j++) {
				System.out.print('*');
			}
			padWidth--;
			System.out.print('\n');
		}
	}
}
