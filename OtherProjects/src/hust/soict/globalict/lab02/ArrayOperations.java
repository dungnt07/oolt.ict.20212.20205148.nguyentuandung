package hust.soict.globalict.lab02;
import java.util.*;

public class ArrayOperations {
	static int[] readInput() {
		Scanner scanner = new Scanner(System.in);
		System.out.println("Enter the array size: ");
		int size = scanner.nextInt();
		int[] arr = new int[size];
		for (int i = 1; i <= size; i++) {
			System.out.print("Enter element " + i + ": ");
			arr[i-1] = scanner.nextInt();
		}
		scanner.close();
		return arr;
	}
	
	static int sumOfArray(int[] arr) {
		int sum = 0;
		for (int i = 0; i < arr.length; i++) {
			sum+= arr[i];
		}
		return sum;
	}
	
	public static void main(String[] args) {
		int[] arr = readInput();
		Arrays.sort(arr);
		System.out.println("Sorted array: ");
		System.out.println(Arrays.toString(arr));
		System.out.println("Sum: ");
		System.out.println(sumOfArray(arr));
		System.out.println("Average value: ");
		System.out.println((double)sumOfArray(arr) / arr.length);
	}
}
