package hust.soict.globalict.lab01;

import javax.swing.JOptionPane;

public class NumericOperations {
    public static void main(String[] args){
        String strNum1 = JOptionPane.showInputDialog(null, "Please input the first number: ", "Input the first number", JOptionPane.INFORMATION_MESSAGE);
        String strNum2 = JOptionPane.showInputDialog(null, "Please input the second number: ", "Input the second number", JOptionPane.INFORMATION_MESSAGE);
        double num1 = Double.parseDouble(strNum1);
        double num2 = Double.parseDouble(strNum2);
        String result = String.format("Sum: %.2f\nDifference: %.2f\nProduct: %.2f\nQuotient: %.2f", num1 + num2, Math.abs(num1 - num2), num1 * num2, num1/num2);
        JOptionPane.showMessageDialog(null, result);
        System.exit(0);
    }
}