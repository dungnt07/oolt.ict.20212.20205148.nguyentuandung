package hust.soict.globalict.lab01;
import java.util.Scanner;

public class InputFromKeyboard {
	public static void main(String[] args) {
		Scanner kb = new Scanner(System.in);
		System.out.println("What's your name?");
		String strName = kb.nextLine();
		System.out.println("How old are you?");
		int iAge = kb.nextInt();
		System.out.println("How tall are you? (m)");
		double dHeight = kb.nextDouble();
		
		System.out.println("Mrs/Ms. " + strName + ", " + iAge + " years old. "
									+ "Your height is " + dHeight + ".");
		kb.close();
	}
}
