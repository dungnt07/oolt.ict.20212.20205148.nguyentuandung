package hust.soict.globalict.lab01;

import javax.swing.JOptionPane;

public class EquationSolver{

    static void firstDegreeOneVarSolver() {
        // Get input
        double a = Double.parseDouble(JOptionPane.showInputDialog(null, "Input a: ", "Input a", JOptionPane.INFORMATION_MESSAGE));
        double b = Double.parseDouble(JOptionPane.showInputDialog(null, "Input b: ", "Input b", JOptionPane.INFORMATION_MESSAGE));

        // Display result
        if (a == 0)
            JOptionPane.showMessageDialog(null,"The equation has no solution.");
        else
            JOptionPane.showMessageDialog(null, String.format("x = %.2f", -b/a));
    }

    static void firstDegreeTwoVarSolver(){
        // Get input
        double a11 = Double.parseDouble(JOptionPane.showInputDialog(null, "Input a11: ", "Input a11", 
                                                                        JOptionPane.INFORMATION_MESSAGE));
        double a12 = Double.parseDouble(JOptionPane.showInputDialog(null, "Input a12: ", "Input a12", 
                                                                        JOptionPane.INFORMATION_MESSAGE));
        double b1 = Double.parseDouble(JOptionPane.showInputDialog(null, "Input b1: ", "Input b1", 
                                                                        JOptionPane.INFORMATION_MESSAGE));
        double a21 = Double.parseDouble(JOptionPane.showInputDialog(null, "Input a21: ", "Input a21", 
                                                                        JOptionPane.INFORMATION_MESSAGE));
        double a22 = Double.parseDouble(JOptionPane.showInputDialog(null, "Input a22: ", "Input a22", 
                                                                        JOptionPane.INFORMATION_MESSAGE));
        double b2 = Double.parseDouble(JOptionPane.showInputDialog(null, "Input b2: ", "Input b2", 
                                                                        JOptionPane.INFORMATION_MESSAGE));

        // Calculate determinants
        double D = a11*a22 - a21*a12;
        double D1 = b1*a22 - b2*a12;
        double D2 = a11*b2 - a21*b1;

        // Display the result based on the value of the determinants
        if (D != 0) 
            JOptionPane.showMessageDialog(null, String.format("x1 = %.2f\nx2 = %.2f", D1/D, D2/D));
        else if (D1 == 0 && D2 == 0)
            JOptionPane.showMessageDialog(null, "The system has infinitely many solutions.");
        else
            JOptionPane.showMessageDialog(null, "The system has no solution.");
    }

    static void secondDegreeSolver(){
        // Get input
        double a = Double.parseDouble(JOptionPane.showInputDialog(null, "Input a: ", "Input a", JOptionPane.INFORMATION_MESSAGE));
        double b = Double.parseDouble(JOptionPane.showInputDialog(null, "Input b: ", "Input b", JOptionPane.INFORMATION_MESSAGE));
        double c = Double.parseDouble(JOptionPane.showInputDialog(null, "Input c: ", "Input c", JOptionPane.INFORMATION_MESSAGE));
        
        double delta = b * b - 4 * a * c;

        // Display the result based on delta value
        if (delta == 0) {
            JOptionPane.showMessageDialog(null, String.format("x = %.2f", -b / (2 * a)));
        }
        else if (delta > 0) {
            JOptionPane.showMessageDialog(null, String.format("x1 = %.2f\nx2 = %.2f", (-b + Math.sqrt(delta)) / (2 * a),
                                                                                 (-b - Math.sqrt(delta)) / (2 * a)));
        }
        else {
            JOptionPane.showMessageDialog(null, "The equation has no solution.");
        }
    }

    public static void main(String[] args){
        String[] options = {"1. First-degree equation with 1 variable", "2. System of first-degree equations with 2 variables", "3. Second-degree equation with one variable"};
        String choiceStr = (String) JOptionPane.showInputDialog(null, "Pick an option", "Menu", JOptionPane.QUESTION_MESSAGE, null, options, options[0]);
        int choice = Character.getNumericValue(choiceStr.charAt(0));
        switch (choice) {
            case 1:
                firstDegreeOneVarSolver();
                break;
            case 2:
                firstDegreeTwoVarSolver();
                break;
            case 3:
                secondDegreeSolver();
                break;
            default:
                break;
        }

        System.exit(0);
    }
}